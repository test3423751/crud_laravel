@extends('layouts.master')

@section('judul')
    Halaman Tambah Category
@endsection

    @section('content')
    <form action="/category" method="POST">
        @csrf
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li class>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  <div class="form-group">
    <label for="exampleInputEmail1">Category Name</label>
    <input type="text" name ="name" class="form-control"> 
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Category Description</label>
    <textarea name="description" class="form-control"></textarea>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

    @endsection