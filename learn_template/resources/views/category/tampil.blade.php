@extends('layouts.master')

@section('judul')
    Halaman Tampil Category
@endsection

    @section('content')

    <a class="btn btn-primary btn-sm my-3" href="/category/create">Tambah</a>

    <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">First</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  
  <tbody>
    <tr>
    @forelse ($categories as $index => $category)
      <th scope="row">{{$index + 1}}</th>
      <td>{{$category->name}}</td>
      <td>
      <form action="/category/{{$category->id}}" method="POST">
	<a href="/category/{{$category->id}}" class="btn btn-info btn-sm">Detail</a>
    <a href="/category/{{$category->id}}->/edit" class="btn btn-warning btn-sm">Edit</a>
    
        @method('delete')
        @csrf
        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
    </form>
	</td>
    </tr>
@empty
<tr>
<td>Tidak ada data Category</td>
<tr>
  </tbody>
@endforelse
</table>

    @endsection