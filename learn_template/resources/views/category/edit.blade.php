@extends('layouts.master')

@section('judul')
    Halaman Tambah Category
@endsection

    @section('content')
    <form action="/category/{{$categories->id}}" method="POST">
        @csrf
        @method('put')
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li class>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  <div class="form-group">
    <label for="exampleInputEmail1">Category Name</label>
    <input type="text" name ="name" value="{{$categories->name}}" class="form-control"> 
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Category Description</label>
    <textarea name="description" class="form-control">{{$categories->description}}</textarea>
  </div>
  <button type="submit" class="btn btn-primary">Update</button>
</form>

    @endsection