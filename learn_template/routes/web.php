<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/master', function () {
    return view('layouts.master');
});

Route::get('/data-table', function () {
    return view('pages.data-table');
});

Route::get('/table', function () {
    return view('pages.table');
});

Route::get('/', function () {
    return view('pages.home');
});

/**
 * C Create Data []
 * R Read Data []
 * U Update Data []
 * D Delete Data []
 */

 // Create Data
 // Menuju ke form Tambah Category
Route::get('/category/create', [CategoryController::class, 'create']);

// Masukkan data ke DB
Route::post('/category', [CategoryController::class, 'store']);

// Read Data
Route::get('/category', [CategoryController::class, 'index']);

Route::get('/category/{id}', [CategoryController::class, 'show']);

Route::get('/category/{id}/edit', [CategoryController::class, 'edit']);

Route::put('/category/{id}', [CategoryController::class, 'update']);

Route::delete('/category/{id}', [CategoryController::class, 'destroy']);