<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function create() {
        return view('category.tambah');
    }
    public function store(Request $request) {
        $request->validate([
            'name' => 'required|min:5',
            'description' => 'required',
        ]);
        DB::table('categories')->insert([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
        ]);
        
        return redirect('/category');
    }

    public function index()
    {
        $categories = DB::table('categories')->get();


        // dd($categories);

        return view('category.tampil', ['categories' => $categories]);
    }

    public function show($id){
        $categories = DB::table('categories')->find($id);

        return view('category.detail', ['categories' => $categories]);
    }

    public function edit($id){
        $categories = DB::table('categories')->find($id);

        return view('category.edit', ['categories' => $categories]);
    
    }

    public function update($id, Request $request)
{

	$request->validate([
		'name' => 'required|min:5', 
		'description' => 'required',
	]);


// Update
	DB::table('categories')
		->where('id', $id)
		->update(
		[
			'name' => $request->input('name'),
			'description' => $request->input('description')
		]
		);
	return redirect('/category');
    }
    public function destroy($id) {
        DB::table('categories')->where('id', $id)->delete();
        return redirect('/category');
    }
}

